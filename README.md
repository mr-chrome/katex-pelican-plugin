# KaTeX Pelican Plugin

Use the **fastest** math typesetting library for the web in Pelican, [KaTeX](https://katex.org/)!

![mdlatex](mdlatex.png)

> - **Fast:** KaTeX renders its math synchronously and doesn’t need to reflow the page.
> - **Print quality:** KaTeX’s layout is based on Donald Knuth’s TeX, the gold standard for math typesetting.
> - **Self contained:** KaTeX has no dependencies and can easily be bundled with your website resources.
> - **Server side rendering:** KaTeX produces the same output regardless of browser or environment, so you can pre-render expressions using Node.js and send them as plain HTML.

This plugin **require** the following to work:

- [markdown_katex](https://pypi.org/project/markdown-katex/)

> `pip install markdown-katex `

- [beautifulsoup4](https://pypi.org/project/beautifulsoup4/)

>`pip install beautifulsoup4`