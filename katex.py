from pelican import signals, contents
from pelican.utils import pelican_open
from bs4 import BeautifulSoup

import re
import markdown_katex
import markdown_katex.extension as ext

"""
Copyleft 2020 Giovanni Crisalfi
KaTeX Pelican Plugin
---------------

Use the fastest math typesetting library for the web in Pelican.
(Mathjax sucks)
"""

math_re = re.compile(r'\${1,2}\s{0,1}.*\s{0,1}\${1,2}')
inline_math_re = re.compile(r'\${1}.*\${1}')

# Generator function
def makeMaths(content):
    if isinstance(content, contents.Static):
        return

    soup = BeautifulSoup(content._content, 'html.parser')

    for paragraph in soup.findAll('p'):
        par_text = paragraph.get_text() # Get stripped text in paragraph
        if math_re.search(par_text):
            for item in math_re.findall(par_text):
                math_par = soup.new_tag('p')
                math_par['class'] = "katex-par"
                math_span = ext.md_inline2html(item.replace("$$", "", 2).replace("\n", "", 2).strip())
                math_par.append(BeautifulSoup(math_span, "html.parser"))

                mitosi_list = par_text.split(item, 1)

                before_math = soup.new_tag('p')
                before_math.string = mitosi_list[0]
                paragraph.replace_with(before_math)

                if len(mitosi_list) > 1:
                    after_math = soup.new_tag('p')
                    after_math.string = mitosi_list[1]
                    before_math.insert_after(after_math)

                before_math.insert_after(math_par)

    soup.renderContents()
    content._content = soup.decode()

def register():
  signals.content_object_init.connect(makeMaths)
